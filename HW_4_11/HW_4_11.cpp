﻿
#include <iostream>
#include <vector>

class Vehicle
{

public:
   
    friend std::ostream& operator<<(std::ostream& out, const Vehicle& v)
    {
        return v.print(out);
    };

    virtual std::ostream& print(std::ostream& out) const = 0;
};

class WaterVehicle : public Vehicle
{

public:

    WaterVehicle(float InBoarding) : Vehicle(), Boarding(InBoarding)
    {
    }

    float Boarding = 0.0f;

    std::ostream& print(std::ostream& out) const override
    {
        out << "Boarding " << Boarding << std::endl;
        return out;
    };
};

class RoadVehicle : public Vehicle
{

public:

    RoadVehicle(float clearance) : Vehicle(), GroundClearance(clearance)
    {

    };    

    std::ostream& print(std::ostream& out) const override
    {
        out << "It's just a road vehicle with clearance = " << GroundClearance;
        return out;
    };

    float GroundClearance;
};

class Wheel
{
public:

    Wheel& operator=(Wheel& other)
    {
        Diameter = other.getDiameter();
        return *this;
    }

    Wheel() :Diameter(0) {};
    Wheel(float Diameter) :Diameter(Diameter) {};
    float getDiameter() const { return Diameter; };

private:

    float  Diameter;
};

class Engine
{
public:

    Engine& operator=(Engine& other)
    {
        Power = other.getPower();
        return *this;
    }

    Engine() :Power(0) {};
    Engine(float Power) :Power(Power) {};
    float getPower() const { return Power; };

private:

    float  Power;
};

class Bicycle : public RoadVehicle
{

public:

    Bicycle(Wheel InFW, Wheel InBW, float InClearance) : RoadVehicle(GroundClearance)
    {
        FW = InFW;
        BW = InBW;
        GroundClearance = InClearance;
    };

    Wheel FW;

    Wheel BW;

    std::ostream& print(std::ostream& out) const override
    {
        {
            out << " Bicycle Wheels: " << FW.getDiameter() << " " << BW.getDiameter() << " "
                << "Ride height: " << GroundClearance;
            return out;
        }
    }
 
};



class Car : public RoadVehicle
{

public:

    Car(Engine InEng, Wheel InFLW, Wheel InFRW, Wheel InBLW, Wheel InBRW, float InClearance) : RoadVehicle(GroundClearance)
    {
        FLeftWheel = InFLW;
        FRightWheel = InFRW;
        BLeftWheel = InBLW;
        BRightWheel = InBRW;
        Eng = InEng;
        GroundClearance = InClearance;
    };

    Wheel FLeftWheel;

    Wheel FRightWheel;

    Wheel BLeftWheel;

    Wheel BRightWheel;

    Engine Eng;
    
    const Engine& getEngine()
    {
        return Eng;
    };

    std::ostream& print(std::ostream& out) const override        
    {
        {
            out << "Car Engine: " << Eng.getPower()
                << " Wheels: "
                << FLeftWheel.getDiameter() << " " << FRightWheel.getDiameter() << " "
                << BLeftWheel.getDiameter() << " " << BRightWheel.getDiameter() << " "
                << "Ride height: " << GroundClearance;
            return out;
        }
    }


};

class Vector
{
public:

    Vector() :x(0), y(0), z(0) 
    {
    };

    Vector(float x, float y, float z) :x(x), y(y), z(z)
    {
    };

    operator float()
    {
        return sqrt(x * x + y * y + z * z);
    };

    float operator[](int index)
    {
        switch (index)
        {
        case 0:
            return x;
            break;
        case 1:
            return y;
            break;
        case 2:
            return z;
            break;
        default:
            std::cout << "Index error: ";
            return 0;
        }
    };

    friend Vector operator+(const Vector& a, const Vector& b)
    {
        return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    friend Vector operator-(const Vector& a, const Vector& b)
    {
        return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    friend Vector operator*(const Vector& a, int mul)
    {
        return Vector(a.x * mul, a.y * mul, a.z * mul);
    }

    friend std::ostream& operator<<(std::ostream& out, const Vector& v)
    {
        out << v.x << " " << v.y << " " << v.z;
        return out;
    }

    friend std::istream& operator>>(std::istream& in, Vector& v)
    {
        std::cout << "Enter X: ";
        in >> v.x;
        std::cout << "Enter Y: ";
        in >> v.y;
        std::cout << "Enter Z: ";
        in >> v.z;

        return in;
    }

    float x;
    float y;
    float z;
};


class Circle : public RoadVehicle
{
public:

    Circle(Vector centr, float Inclearance) : RoadVehicle(Inclearance)
    {
        centrPoint = centr;
    };



    std::ostream& print(std::ostream& out) const override
    {
        out << "Circle center: " << centrPoint << " " << "Ride height:" << " " << GroundClearance;

        return out;
    };

    Vector centrPoint;
};


float getHighestPower(std::vector<Vehicle*> v)
{
    float max = 0;
    for (auto it : v)
    {
        if (dynamic_cast<Car*>(it) != nullptr)
        {
            float power = dynamic_cast<Car*>(it)->getEngine().getPower();
            max = (max > power) ? max : power;
        }
    }

    return max;
};

int main()
{
    Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150);

    std::cout << c << '\n';

    Bicycle t(Wheel(20), Wheel(20), 300);

    std::cout << t << '\n';    

    Vector Point;

    std::vector<Vehicle*> v;

    v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));

    v.push_back(new Circle(Vector(1, 2, 3), 7));

    v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));

    v.push_back(new WaterVehicle(5000));

    std::cout << "\n";

    for (auto it : v)
    {
        std::cout << *it << "\n";
    }


    std::cout << "The highest power is " << getHighestPower(v) << '\n';

    for (auto it : v)
    {
        delete it;
    }
    v.clear();
}

